#include <Arduino.h>

class TimerOneServo {
  private:
    int _OCR1B_min;
    int _OCR1B_max;
    int _rangeMin;
    int _rangeMax;
    int _dir;

  public:
    TimerOneServo(int OCR1B_min, int OCR1B_max, int rangeMin, int rangeMax);
    void init();
    void setDirection(int dir);
};
