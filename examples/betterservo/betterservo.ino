#include "TimerOneServo.h"

// Find min and max compare register values to get 1 ms to 2 ms pulse
// Using 16-bit timer with prescale of 8
// Register value should be number of clock cycles before reset - 1 b/c we count up from 0
int OCR1B_min = 1099; //+1=2000 clock cycles to get 1 ms pulse width (off)
int OCR1B_max = 4899; // to get 2 ms pulse width (max speed)

int pos = 0;

TimerOneServo servo = TimerOneServo(OCR1B_min, OCR1B_max, 0, 1023); // OCR1B_min, OCR1B_max, rangeMin, rangeMax

void setup() {
  servo.init();
}

void loop() {
  
  for (pos = 0; pos <= 1023; pos += 1) { // goes from 0 degrees to 180 degrees
    servo.setDirection(pos);
    delay(5);                       // waits 15ms for the servo to reach the position
  }
  for (pos = 1023; pos >= 0; pos -= 1) { // goes from 180 degrees to 0 degrees
    servo.setDirection(pos);
    delay(5);                       // waits 15ms for the servo to reach the position
  }
  
  
}
